#!/usr/bin/env python3
import os

import aws_cdk as cdk

from alert_rcb_v2_cdk.alert_rcb_v2_cdk_stack import AlertRcbV2CdkStack


app = cdk.App()
AlertRcbV2CdkStack(
    app,
    "AlertRcbV2CdkStack",
    env=cdk.Environment(
        account=os.environ.get("CDK_DEPLOY_ACCOUNT", os.environ["CDK_DEFAULT_ACCOUNT"]),
        region=os.environ.get("CDK_DEPLOY_REGION", os.environ["CDK_DEFAULT_REGION"]),
    ),
)

app.synth()
