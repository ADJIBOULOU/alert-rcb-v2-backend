import aws_cdk as core
import aws_cdk.assertions as assertions

from alert_rcb_v2_cdk.alert_rcb_v2_cdk_stack import AlertRcbV2CdkStack

# example tests. To run these tests, uncomment this file along with the example
# resource in alert_rcb_v2_cdk/alert_rcb_v2_cdk_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = AlertRcbV2CdkStack(app, "alert-rcb-v2-cdk")
    template = assertions.Template.from_stack(stack)


#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
