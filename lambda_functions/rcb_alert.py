import boto3
import json
from encoder import Encoder

dynamodbTableName = "AlertRCB"
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(dynamodbTableName)

getMethod = "GET"
postMethod = "POST"
deleteMethod = "DELETE"
rcbAlertPath = "/rcbalert"
rcbAlertsPath = "/rcbalerts"


def lambda_handler(event, context):
    httpMethod = event["httpMethod"]
    path = event["path"]
    if httpMethod == getMethod and path == rcbAlertPath:
        response = getRcbAlert(event["queryStringParameters"]["rcbAlertId"])
    elif httpMethod == getMethod and path == rcbAlertsPath:
        response = getRcbAlerts()
    elif httpMethod == postMethod and path == rcbAlertPath:
        requestBody = json.loads(event["body"])
        response = addRcbAlert(requestBody)
    elif httpMethod == deleteMethod and path == rcbAlertPath:
        requestBody = json.loads(event["body"])
        response = deleteRcbAlert(requestBody["rcbAlertId"])
    else:
        response = buildResponse(404, "Not Found")

    return response


def getRcbAlert(rcbAlertId):
    response = table.get_item(Key={"rcbAlertId": rcbAlertId})
    if "Item" in response:
        return buildResponse(200, response["Item"])
    else:
        return buildResponse(
            404, {"Message": f"RCB Alert with id {rcbAlertId} does not exist"}
        )


def getRcbAlerts():
    response = table.scan()
    result = response["Items"]

    returnBody = {"RCBAlerts": result}
    return buildResponse(200, returnBody)


def addRcbAlert(requestBody):
    table.put_item(Item=requestBody)
    body = {"Operation": "ADD", "Message": "SUCCESS", "Item": requestBody}
    return buildResponse(200, body)


def deleteRcbAlert(rcbAlertId):
    response = table.delete_item(Key={"rcbAlertId": rcbAlertId}, ReturnValues="ALL_OLD")
    body = {"Operation": "DELETE", "Message": "SUCCESS", "DeletedAlert": response}
    return buildResponse(200, body)


def buildResponse(statusCode, body=None):
    response = {
        "statusCode": statusCode,
    }
    if body is not None:
        response["body"] = json.dumps(body, cls=Encoder)
    return response
