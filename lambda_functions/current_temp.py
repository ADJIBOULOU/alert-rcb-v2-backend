import json
import urllib3
from encoder import Encoder

cities_dict = {
    "Wrocław": [51.110, 17.030],
    "Bydgoszcz": [53.120, 18],
    "Lublin": [51.240, 22.570],
    "Gorzów Wielkopolski": [52.740, 15.230],
    "Łódź": [51.7833, 19.4667],
    "Kraków": [50.060, 19.959],
    "Warszawa": [52.259, 21.020],
    "Opole": [50.679, 17.940],
    "Rzeszów": [50.049, 21.999],
    "Białystok": [53.139, 23.159],
    "Gdańsk": [54.360, 18.639],
    "Katowice": [50.259, 19.020],
    "Kielce": [50.889, 20.649],
    "Olsztyn": [53.779, 20.489],
    "Poznań": [52.399, 16.900],
    "Szczecin": [52.430, 14.529],
}
api_key = "6460a0c609847b7c35735393afa8b776"
get_method = "GET"
current_temp_path = "/current"
http = urllib3.PoolManager()


def lambda_handler(event, context):
    http_method = event["httpMethod"]
    path = event["path"]
    if http_method == get_method and path == current_temp_path:
        response = get_current_temperatures()
    else:
        response = build_response(404, "Not Found")

    return response


def get_current_temperatures():
    result = []
    for city_name in cities_dict.keys():
        geographical_coordinates = cities_dict[city_name]
        url = (
            f"http://api.openweathermap.org/data/2.5/onecall?lat={geographical_coordinates[0]}2&"
            + f"lon={geographical_coordinates[1]}&exclude=hourly,daily,alerts,minutely&units=metric&appid={api_key}"
        )
        response = http.request(get_method, url)
        response_json = json.loads(response.data.decode("utf-8"))
        current_temperature = response_json["current"]["temp"]
        result.append([city_name, current_temperature])
    return_body = {"Current temperatures": result}
    return build_response(200, return_body)


def build_response(status_code, body=None):
    response = {
        "statusCode": status_code,
    }
    if body is not None:
        response["body"] = json.dumps(body, cls=Encoder)
    return response
