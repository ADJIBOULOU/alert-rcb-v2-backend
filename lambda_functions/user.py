import boto3
import json
from encoder import Encoder

dynamodbTableName = "User"
dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(dynamodbTableName)

getMethod = "GET"
postMethod = "POST"
patchMethod = "PATCH"
deleteMethod = "DELETE"
userPath = "/user"


def lambda_handler(event, context):
    httpMethod = event["httpMethod"]
    path = event["path"]
    if httpMethod == getMethod and path == userPath:
        response = getUserInformation(event["queryStringParameters"]["login"])
    elif httpMethod == postMethod and path == userPath:
        requestBody = json.loads(event["body"])
        response = addUser(requestBody)
    elif httpMethod == patchMethod and path == userPath:
        requestBody = json.loads(event["body"])
        response = editUserInformation(
            requestBody["login"], requestBody["updateKey"], requestBody["updateValue"]
        )
    elif httpMethod == deleteMethod and path == userPath:
        requestBody = json.loads(event["body"])
        response = deleteUser(requestBody["login"])
    else:
        response = buildResponse(404, "Not Found")

    return response


def getUserInformation(login):
    response = table.get_item(Key={"login": login})
    if "Item" in response:
        return buildResponse(200, response["Item"])
    else:
        return buildResponse(
            404, {"Message": f"User with login {login} does not exist"}
        )


def addUser(requestBody):
    table.put_item(Item=requestBody)
    body = {"Operation": "ADD", "Message": "SUCCESS", "Item": requestBody}
    return buildResponse(200, body)


def editUserInformation(login, updateKey, updateValue):
    response = table.update_item(
        Key={"login": login},
        UpdateExpression=f"set {updateKey} = :value",
        ExpressionAttributeValues={":value": updateValue},
        ReturnValues="UPDATED_NEW",
    )
    body = {"Operation": "UPDATE", "Message": "SUCCESS", "UpdatedAttributes": response}
    return buildResponse(200, body)


def deleteUser(login):
    response = table.delete_item(Key={"login": login}, ReturnValues="ALL_OLD")
    body = {"Operation": "DELETE", "Message": "SUCCESS", "DeletedAlert": response}
    return buildResponse(200, body)


def buildResponse(statusCode, body=None):
    response = {
        "statusCode": statusCode,
    }
    if body is not None:
        response["body"] = json.dumps(body, cls=Encoder)
    return response
