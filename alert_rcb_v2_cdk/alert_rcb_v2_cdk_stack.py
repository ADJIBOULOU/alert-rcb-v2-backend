from aws_cdk import (
    # Duration,
    Stack,
    aws_dynamodb as _dynamodb,
    # aws_sqs as sqs,
    aws_lambda as _lambda,
    aws_apigateway as apigw,
)
from constructs import Construct


class AlertRcbV2CdkStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        user = _dynamodb.Table(
            self,
            id="User",
            table_name="User",
            partition_key=_dynamodb.Attribute(
                name="login", type=_dynamodb.AttributeType.STRING
            ),
        )

        alertrcb = _dynamodb.Table(
            self,
            id="AlertRCB",
            table_name="AlertRCB",
            partition_key=_dynamodb.Attribute(
                name="rcbAlertId", type=_dynamodb.AttributeType.STRING
            ),
        )

        alerttemp = _dynamodb.Table(
            self,
            id="AlertTemp",
            table_name="AlertTemp",
            partition_key=_dynamodb.Attribute(
                name="tempAlertId", type=_dynamodb.AttributeType.STRING
            ),
        )

        temp_alert_lambda = _lambda.Function(
            self,
            "TempAlertHandler",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("lambda_functions"),
            handler="temp_alert.lambda_handler",
        )
        temp_alert_lambda.add_environment("TABLE_NAME", alerttemp.table_name)
        alerttemp.grant_full_access(temp_alert_lambda)

        apigw.LambdaRestApi(
            self,
            "TempAlertEndpoint",
            handler=temp_alert_lambda,
        )

        rcb_alert_lambda = _lambda.Function(
            self,
            "RCBAlertHandler",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("lambda_functions"),
            handler="rcb_alert.lambda_handler",
        )
        rcb_alert_lambda.add_environment("TABLE_NAME", alertrcb.table_name)
        alertrcb.grant_full_access(rcb_alert_lambda)

        apigw.LambdaRestApi(
            self,
            "RCBAlertEndpoint",
            handler=rcb_alert_lambda,
        )

        user_lambda = _lambda.Function(
            self,
            "UserHandler",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("lambda_functions"),
            handler="user.lambda_handler",
        )
        user_lambda.add_environment("TABLE_NAME", user.table_name)
        user.grant_full_access(user_lambda)

        apigw.LambdaRestApi(
            self,
            "UserAlertEndpoint",
            handler=user_lambda,
        )

        current_temp_lambda = _lambda.Function(
            self,
            "CurrentTempHandler",
            runtime=_lambda.Runtime.PYTHON_3_9,
            code=_lambda.Code.from_asset("lambda_functions"),
            handler="current_temp.lambda_handler",
        )

        apigw.LambdaRestApi(
            self,
            "CurrentTempAlertEndpoint",
            handler=current_temp_lambda,
        )
